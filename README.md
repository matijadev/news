Table of Contents
=================
   * [News App](#news-app)
    * [Technology stack](#technology-stack)
    * [Install](#install)
    * [Code Style](#code-style)
        * [File template](#file-template)
        * [File structure](#file-structure)
#  News app Client
> a frontend implementation of a news-app
## Technology stack
    - ReactJS
    - Babel
    - Webpack
    - SCSS
    - ReactRouter
    - Axios
    - Prettier
    - Linter
## Install
Please run the "install" command first:
```sh
$ npm/yarn install
```
This will install all of the required dependencies, as stated in `package.json`file.
To start the server, run:
```sh
$ npm/yarn start
```
which will start `webpack` server in DEV mode.
To build the project, run:
```sh
$ npm/yarn build
```
This will build the project in dist/main.js.
## Code Style
> Some general notes on Code Style used for this project
> - all global styles -> /App/variables.scss
> - Every component is module and styles are scoped only to that component
### File template
Each component should be in the separate folder in src/:
```js
src
        componentFolder
            component.js
            component.scss
```
Component which are usualy reusable should go in src/common:
```js
src
    components
        commonFolder
```

#### Config
> switch keys in /src/config/config.js if the backend API reaches maximum requests

* **Matija Mišić** - (https://github.com/MatijaMisic)