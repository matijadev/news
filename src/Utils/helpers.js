/* eslint-disable no-plusplus */
/* eslint-disable import/prefer-default-export */
export const registerAllCalls = (call, categories, language) => {
  const ps = [];
  for (let i = 0; i < categories.length; i++) {
    ps.push(
      call(language, categories[i]).then((res) => {
        return { category: categories[i], articles: res.articles };
      })
    );
  }

  return Promise.all(ps);
};

export const remapResponse = (arr) => {
  const obj = {};

  for (let i = 0; i < arr.length; i++) {
    obj[arr[i].category] = arr[i].articles;
  }

  return obj;
};

export const filterSingleNews = (categoryNews, searchParam) => {
  return Object.values(categoryNews)
    .flat(1)
    .find((news) => news.title === searchParam);
};
