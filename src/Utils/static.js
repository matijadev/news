/* eslint-disable import/prefer-default-export */
export const categories = [
  'business',
  'entertainment',
  'general',
  'health',
  'science',
  'sports',
  'technology',
];

export const sliderSettings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 5,
  arrows: true,
};

export const pageUnknown = "We couldn't find the page you are looking for!";
export const errorMessage = 'Oopss something went wrong...';
