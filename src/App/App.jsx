import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { pageUnknown } from '../Utils/static';
import Header from './Common/Header/Header';
import TopNews from './Pages/TopNews/TopNews';
import Search from './Pages/Search/Search';
import Categories from './Pages/Categories/Categories';
import ErrorPage from './Pages/ErrorPage/ErrorPage';
import './App.module.scss';

const App = () => {
  return (
    <>
      <Header />
      <Switch>
        <Route exact path="/categories/" component={Categories} />
        <Route exact path="/singleNews/categories/:id" component={Categories} />
        <Route
          exact
          path="/selectedCategory/categories/:id"
          component={Categories}
        />
        <Route exact path="/search/" component={Search} />
        <Route exact path="/singleNews/search/:id" component={Search} />
        <Route exact path="/" component={TopNews} />
        <Route exact path="/singleNews/:id" component={TopNews} />
        <Route render={() => <ErrorPage message={pageUnknown} />} />
      </Switch>
    </>
  );
};

export default App;
