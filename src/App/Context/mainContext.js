/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-use-before-define */
import React, { useContext, useState } from 'react';

const MainContext = React.createContext();

const MainContextProvider = ({ children }) => {
  const [language, setLanguage] = useState(
    localStorage.getItem('preferedLanguage') || 'US'
  );
  const [topNews, setNews] = useState();
  const [searchNews, setSearch] = useState();
  const [categoriesNews, setCategories] = useState();

  const topNewsContext = {
    topNews,
    setNews,
  };

  const languageContext = {
    language,
    setLanguage,
  };

  const searchNewsContext = {
    searchNews,
    setSearch,
  };

  const categoriesNewsContext = {
    categoriesNews,
    setCategories,
  };

  // TODO split GLOBAL CONTEXT into separate files
  const value = {
    topNewsContext,
    languageContext,
    searchNewsContext,
    categoriesNewsContext,
  };
  return <MainContext.Provider value={value}>{children}</MainContext.Provider>;
};

export default MainContextProvider;

export const useMainContext = () => useContext(MainContext);
