/* eslint-disable no-use-before-define */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { useMainContext } from '../../Context/mainContext';
import { getTopNewsForGivenLanguage } from '../../../services/requestServices';
import { errorMessage, pageUnknown } from '../../../Utils/static';
import NewsList from '../../Common/NewsList/NewsList';
import SingleNews from '../../Common/SingleNews/SingleNews';
import ErrorPage from '../ErrorPage/ErrorPage';
import Spinner from '../../Common/Spinner/Spinner';
import styles from './TopNews.module.scss';

const TopNews = (props) => {
  const context = useMainContext();
  const [error, setError] = useState(false);
  const { topNewsContext, languageContext } = context;
  const {
    match: { params },
  } = props;
  const { topNews, setNews } = topNewsContext;
  const { language } = languageContext;

  useEffect(() => {
    if (!topNews) fetchData();
  }, []);

  useEffect(() => {
    if (topNews) fetchData();
  }, [language]);

  function fetchData() {
    getTopNewsForGivenLanguage(language)
      .then((res) => {
        setNews(res.articles);
      })
      .catch(() => {
        setError(true);
      });
  }

  if (error) {
    return <ErrorPage message={errorMessage} />;
  }

  // if the user clicks on the single news, filter and display
  if (Object.keys(params).length && topNews) {
    const matchNews = topNews.find((news) => news.title === params.id);
    return matchNews ? (
      <SingleNews news={matchNews} page="topNews" />
    ) : (
      <ErrorPage message={pageUnknown} />
    );
  }

  if (topNews) {
    return (
      <div className={styles.topNews}>
        <h1>{`Top news from ${language}`}</h1>
        <div>
          <NewsList newsList={topNews} page="topNews" />
        </div>
      </div>
    );
  }

  return <Spinner />;
};

export default TopNews;
