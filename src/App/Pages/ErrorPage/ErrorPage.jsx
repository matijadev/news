import React from 'react';
import errorIcon from '../../../assets/Img/error.svg';
import styles from './ErrorPage.module.scss';

const ErrorPage = ({ message }) => {
  return (
    <div className={styles.errorPage}>
      <img src={errorIcon} alt="Something wrong" />
      <h3>{message}</h3>
    </div>
  );
};

export default ErrorPage;
