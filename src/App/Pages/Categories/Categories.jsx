/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-use-before-define */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { useMainContext } from '../../Context/mainContext';
import { getCategoriesForGivenLanguage } from '../../../services/requestServices';
import { categories, errorMessage, pageUnknown } from '../../../Utils/static';
import {
  registerAllCalls,
  remapResponse,
  filterSingleNews,
} from '../../../Utils/helpers';
import NewsList from '../../Common/NewsList/NewsList';
import SingleNews from '../../Common/SingleNews/SingleNews';
import ErrorPage from '../ErrorPage/ErrorPage';
import Spinner from '../../Common/Spinner/Spinner';
import styles from './Categories.module.scss';
import Category from '../../Common/Category/Category';

const Categories = (props) => {
  const context = useMainContext();
  const { categoriesNewsContext, languageContext } = context;
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const { language } = languageContext;
  const { categoriesNews, setCategories } = categoriesNewsContext;

  const {
    match: { params },
  } = props;
  useEffect(() => {
    if (!categoriesNews) {
      fetchData();
    }
    return () => {
      setCategories();
    };
  }, []);

  useEffect(() => {
    if (categoriesNews) {
      fetchData();
    }
  }, [language]);

  function fetchData() {
    setLoading(true);
    registerAllCalls(getCategoriesForGivenLanguage, categories, language)
      .then((res) => {
        setCategories(remapResponse(res));
        setLoading(false);
      })
      .catch(() => {
        setError(true);
      });
  }

  if (error) {
    return <ErrorPage message={errorMessage} />;
  }

  // if the user clicks on the single news, filter and display
  if (
    Object.keys(params).length &&
    categoriesNews &&
    !categoriesNews[params.id]
  ) {
    const matchNews = filterSingleNews(categoriesNews, params.id);
    return matchNews ? (
      <SingleNews news={matchNews} page="categories" />
    ) : (
      <ErrorPage message={pageUnknown} />
    );
  }

  if (loading || !categoriesNews) {
    return <Spinner />;
  }

  return (
    <div className={styles.categoryPage}>
      {categoriesNews[params.id] ? (
        <h1>{`Top ${params.id} news by categories from ${language}`}</h1>
      ) : (
        <h1>{`Top 5 news by categories from ${language}`}</h1>
      )}
      {categoriesNews[params.id] ? (
        <div className={styles.oneCategory}>
          <NewsList newsList={categoriesNews[params.id]} page="categories" />
        </div>
      ) : (
        categories.map((category) => (
          <Category key={category} news={categoriesNews} category={category} />
        ))
      )}
    </div>
  );
};

export default Categories;
