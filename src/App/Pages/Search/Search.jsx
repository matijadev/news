/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { getSearchForGivenKeword } from '../../../services/requestServices';
import { useMainContext } from '../../Context/mainContext';
import { errorMessage, pageUnknown } from '../../../Utils/static';
import styles from './Search.module.scss';
import NewsList from '../../Common/NewsList/NewsList';
import SingleNews from '../../Common/SingleNews/SingleNews';
import ErrorPage from '../ErrorPage/ErrorPage';

const Search = (props) => {
  const context = useMainContext();
  const [searchKeyword, setKeyword] = useState('');
  const [error, setError] = useState(false);

  const {
    match: { params },
  } = props;
  const {
    searchNewsContext: { searchNews, setSearch },
    languageContext: { language },
  } = context;

  function inputHandler(e) {
    setKeyword(e.target.value);
  }

  useEffect(() => {
    if (searchKeyword)
      getSearchForGivenKeword(searchKeyword)
        .then((res) => {
          setSearch(res.articles);
        })
        .catch(() => {
          setError(true);
        });
  }, [searchKeyword]);

  if (error) {
    return <ErrorPage message={errorMessage} />;
  }

  // if the user clicks on the single news, filter and display
  if (Object.keys(params).length && searchNews) {
    const matchNews = searchNews.find((news) => news.title === params.id);
    return matchNews ? (
      <SingleNews news={matchNews} page="search" />
    ) : (
      <ErrorPage message={pageUnknown} />
    );
  }

  return (
    <div className={styles.searchPage}>
      <h1>{`Search top news from ${language} by term:`}</h1>
      <input
        aria-label="Keyword search field"
        type="text"
        onChange={inputHandler}
      />
      {searchNews ? (
        <div>
          <NewsList newsList={searchNews} page="search" />
        </div>
      ) : null}
    </div>
  );
};

export default Search;
