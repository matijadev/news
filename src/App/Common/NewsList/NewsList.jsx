import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import NewsCard from '../NewsCard/NewsCard';

const NewsList = ({ newsList, page }) => {
  function renderNewsList() {
    return newsList.map((news) => {
      return <NewsCard key={uuidv4()} news={news} page={page} />;
    });
  }

  return renderNewsList();
};

export default NewsList;
