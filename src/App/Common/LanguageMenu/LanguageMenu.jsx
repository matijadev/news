import React from 'react';
import { useLocation } from 'react-router-dom';
import { useMainContext } from '../../Context/mainContext';
import style from './LanguageMenu.module.scss';

const LangugeMenu = () => {
  const context = useMainContext();
  const {
    languageContext: { setLanguage, language },
  } = context;
  const { pathname } = useLocation();
  const disableButtons = pathname.split('/')[1] === 'singleNews';

  function saveLanguage(e) {
    localStorage.setItem('preferedLanguage', e.target.name);
    setLanguage(e.target.name);
  }
  return (
    <div className={style.languageButtons}>
      <button
        className={language === 'GB' ? style.activeButton : ''}
        type="button"
        name="GB"
        onClick={saveLanguage}
        disabled={disableButtons}
      >
        GB
      </button>
      <button
        className={language === 'US' ? style.activeButton : ''}
        type="button"
        name="US"
        onClick={saveLanguage}
        disabled={disableButtons}
      >
        US
      </button>
    </div>
  );
};

export default LangugeMenu;
