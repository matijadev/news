import React from 'react';
import HeaderNav from '../HeaderNav/HeaderNav';
import styles from './Header.module.scss';
import LangugeMenu from '../LanguageMenu/LanguageMenu';

const Header = () => {
  return (
    <header>
      <div>
        <HeaderNav title="top news" url="/" />
        <HeaderNav title="categories" url="/categories" />
        <HeaderNav title="search" url="/search" />
      </div>
      <div className={styles.languageHeader}>
        <LangugeMenu />
      </div>
    </header>
  );
};

export default Header;
