import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import Icon from '../Icon/Icon';
import NewsList from '../NewsList/NewsList';
import styles from './Slider.module.scss';

const Slider = ({ news }) => {
  const [slide, setSlide] = useState(0);
  const id = uuidv4();

  function nextSlide() {
    const x = document.getElementById(id).style.transform;
    const getNum = x.match(/(\d+)/)[0];
    if (getNum > (news.length - 2) * 330) return;
    setSlide((prevState) => prevState - 330);
  }

  function prevSlide() {
    if (slide === 0) return;
    setSlide((prevState) => prevState + 330);
  }

  return (
    <>
      <div className={styles.singleCategorySlider}>
        <div
          id={id}
          style={{ transform: `translateX(${slide}px)` }}
          className={styles.singleCategory}
        >
          <NewsList newsList={news} page="categories" />
        </div>
      </div>
      <div className={styles.sliderButtons}>
        <button
          type="button"
          onClick={prevSlide}
          aria-label="move slider to the left"
        >
          <Icon name="arrow" />
        </button>
        <button
          type="button"
          onClick={nextSlide}
          aria-label="move slider to the right"
        >
          <Icon name="arrow" />
        </button>
      </div>
    </>
  );
};

export default Slider;
