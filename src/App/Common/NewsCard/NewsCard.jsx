import React from 'react';
import { Link } from 'react-router-dom';
import Icon from '../Icon/Icon';
import styles from './NewsCard.module.scss';
import noImage from '../../../assets/Img/noimage.svg';

const NewsCard = ({ news, page }) => {
  const { title, urlToImage, description } = news;
  let url;

  switch (page) {
    case 'topNews':
      url = `/singleNews/${title.replace(/%/g, '')}`;
      break;
    case 'search':
      url = `/singleNews/search/${title.replace(/%/g, '')}`;
      break;
    case 'categories':
      url = `/singleNews/categories/${title.replace(/%/g, '')}`;
      break;
    default:
  }

  return (
    <div className={styles.newsCard}>
      <div>
        <h2>{title}</h2>
      </div>
      <img src={urlToImage || noImage} alt="Single news" />
      <p>
        {description}
        ..
      </p>
      <Link to={url} aria-label="Link to single page">
        <Icon name="read more" />
      </Link>
    </div>
  );
};

export default NewsCard;
