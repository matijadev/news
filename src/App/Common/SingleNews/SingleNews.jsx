import React from 'react';
import { useHistory } from 'react-router-dom';
import Icon from '../Icon/Icon';
import noImage from '../../../assets/Img/noimage.svg';
import styles from './SingleNews.module.scss';

const SingleNews = ({ news }) => {
  const { title, urlToImage, description } = news;
  const history = useHistory();
  function goBack() {
    history.goBack();
  }

  return (
    <div className={styles.singleNews}>
      <h1>{title}</h1>
      <img src={urlToImage || noImage} alt="Selected top news" />
      <p>{description}</p>
      <button type="button" onClick={goBack}>
        <Icon name="return" />
      </button>
      <Icon />
    </div>
  );
};

export default SingleNews;
