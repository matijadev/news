/* eslint-disable no-use-before-define */
import React from 'react';

const SVGIcon = ({
  name = '',
  style = {},
  fill = '#000',
  viewBox = '',
  width = '100%',
  className = '',
  height = '100%',
}) => (
  <svg
    width={width}
    style={style}
    height={height}
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    viewBox={viewBox || getViewBox(name)}
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    {getPath(name, { fill })}
  </svg>
);
export default SVGIcon;
const getViewBox = (name) => {
  switch (name) {
    case 'top news':
    case 'categories':
    case 'search':
    case 'read more':
    case 'arrow':
    case 'return':
      return '0 0 24 24';
    default:
      return '0 0 32 32';
  }
};
const getPath = (name) => {
  switch (name) {
    case 'top news':
      return (
        <>
          <g>
            <path d="M19,5v14H5V5H19 M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3L19,3z" />
          </g>
          <path d="M14,17H7v-2h7V17z M17,13H7v-2h10V13z M17,9H7V7h10V9z" />
        </>
      );
    case 'categories':
      return (
        <>
          <path d="M0 0h24v24H0V0z" fill="none" />
          <path d="M3 17h18v2H3zm16-5v1H5v-1h14m2-2H3v5h18v-5zM3 6h18v2H3z" />
        </>
      );
    case 'search':
      return (
        <>
          <path d="M0 0h24v24H0V0z" fill="none" />
          <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
        </>
      );
    case 'read more':
      return (
        <g>
          <rect height="2" width="9" x="13" y="7" />
          <rect height="2" width="9" x="13" y="15" />
          <rect height="2" width="6" x="16" y="11" />
          <polygon points="13,12 8,7 8,11 2,11 2,13 8,13 8,17" />
        </g>
      );
    case 'arrow':
      return (
        <>
          <path d="M0 0h24v24H0z" fill="none" />
          <path d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z" />
        </>
      );
    case 'return':
      return (
        <>
          <path d="M0 0h24v24H0z" fill="none" />
          <path d="M19 7v4H5.83l3.58-3.59L8 6l-6 6 6 6 1.41-1.41L5.83 13H21V7z" />
        </>
      );
    default:
      return <path />;
  }
};
