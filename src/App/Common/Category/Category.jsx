import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Icon from '../Icon/Icon';
import Slider from '../Slider/Slider';
import styles from './Category.module.scss';

const Category = ({ news, category }) => {
  const history = useHistory();
  const [isShow, setShow] = useState(true);
  function pushTosingleCateogy() {
    history.push(`selectedCategory/categories/${category}`);
  }

  function toggleCategory() {
    setShow((prevState) => !prevState);
  }

  return (
    <div key={category}>
      <div className={styles.categoryHeading}>
        <button
          type="button"
          onClick={pushTosingleCateogy}
          aria-label="goto selected category full list news"
        >
          {category}
        </button>
        <button
          type="button"
          className={`${isShow ? '' : styles.rotateIcon}`}
          onClick={toggleCategory}
          aria-label="open close category"
        >
          <Icon name="arrow" />
        </button>
      </div>
      {isShow ? <Slider news={news[category]} /> : null}
    </div>
  );
};

export default Category;
