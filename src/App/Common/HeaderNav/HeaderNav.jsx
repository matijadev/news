import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import styles from './HeaderNav.module.scss';
import Icon from '../Icon/Icon';

const HeaderNav = ({ title, url }) => {
  const location = useLocation().pathname;
  const isActive = location === url ? styles.activeIcon : '';

  return (
    <Link to={url}>
      <div className={`${styles.headerNav} ${isActive}`}>
        <Icon name={title} />
        <p>{title}</p>
      </div>
    </Link>
  );
};

export default HeaderNav;
