import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import App from './App/App';
import MainContextProvider from './App/Context/mainContext';

ReactDOM.render(
  <HashRouter>
    <MainContextProvider>
      <App />
    </MainContextProvider>
  </HashRouter>,
  document.getElementById('root')
);
