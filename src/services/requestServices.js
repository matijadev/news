/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import { apiKey } from '../config/config';

export const getTopNewsForGivenLanguage = (country) => {
  const url = `https://newsapi.org/v2/top-headlines?country=${country}&apiKey=${apiKey}`;
  return axios.get(url).then((res) => res.data);
};

export const getSearchForGivenKeword = (keyword) => {
  const url = `https://newsapi.org/v2/everything?q=${keyword}&apiKey=${apiKey}`;
  return axios.get(url).then((res) => res.data);
};

export const getCategoriesForGivenLanguage = (country, category) => {
  const url = `https://newsapi.org/v2/top-headlines?country=${country}&category=${category}&apiKey=${apiKey}`;
  return axios.get(url).then((res) => res.data);
};
